package ee.applaud.smartlab.robotiumspoontest.test;

import android.app.Activity;
import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;

import com.robotium.solo.Solo;

import ee.applaud.smartlab.robotiumspoontest.test.utils.TestUtils;

/**
 * Created by Dmitri Bogatenkov on 12/3/2015.
 */
public class BaseTestCase<T extends Activity> extends ActivityInstrumentationTestCase2<T> {

    protected Solo mSolo;
    protected Context mContext;

    public BaseTestCase(Class<T> activityClass) {
        super(activityClass);
    }


    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mSolo = new Solo(getInstrumentation(), getActivity());
        mContext = getActivity().getApplicationContext();
    }


    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        mSolo.finishOpenedActivities();
        try {
            mSolo.finalize();
        } catch (Throwable e) {
            TestUtils.writeLogCat(e.getMessage());
        }
    }
}
