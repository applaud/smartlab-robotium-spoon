package ee.applaud.smartlab.robotiumspoontest.test.tests;

import com.squareup.spoon.Spoon;

import ee.applaud.smartlab.robotiumspoontest.NotesList;
import ee.applaud.smartlab.robotiumspoontest.test.BaseTestCase;

/**
 * Created by Dmitri Bogatenkov on 12/3/2015.
 */
public class NotePadTest extends BaseTestCase<NotesList> {

    public NotePadTest() {
        super(NotesList.class);
    }

    public void testAddNote() throws Exception {
        Spoon.screenshot(getActivity(), "initial_state");
        //Unlock the lock screen
        mSolo.unlockScreen();
        mSolo.clickOnMenuItem("Add note");
        //Assert that NoteEditor activity is opened
        mSolo.assertCurrentActivity("Expected NoteEditor activity", "NoteEditor");
        //In text field 0, enter Note 1
        mSolo.enterText(0, "Note 1");
        mSolo.goBack();
        //Clicks on menu item
        mSolo.clickOnMenuItem("Add note");
        //In text field 0, type Note 2
        mSolo.typeText(0, "Note 2");
        //Go back to first activity
        mSolo.goBack();
        Spoon.screenshot(getActivity(), "2_notes_added");

        boolean notesFound = mSolo.searchText("Note 1") && mSolo.searchText("Note 2");
        //Assert that Note 1 & Note 2 are found
        assertTrue("Note 1 and/or Note 2 are not found", notesFound);
    }

    public void testEditNote() throws Exception {
        // Click on the second list line
        mSolo.clickInList(2);
        //Hides the soft keyboard
        mSolo.hideSoftKeyboard();
        // Change orientation of activity
        mSolo.setActivityOrientation(mSolo.LANDSCAPE);
        // Change title
        mSolo.clickOnMenuItem("Edit title");
        //In first text field (0), add test
        mSolo.enterText(0, " test");
        mSolo.goBack();

        Spoon.screenshot(getActivity(), "1_note_title_edited");
        mSolo.setActivityOrientation(mSolo.PORTRAIT);
        // (Regexp) case insensitive
        boolean noteFound = mSolo.waitForText("(?i).*?note 1 test");
        //Assert that Note 1 test is found
        assertTrue("Note 1 test is not found", noteFound);

    }

    public void testRemoveNote() throws Exception {
        //(Regexp) case insensitive/text that contains "test"
        mSolo.clickOnText("(?i).*?test.*");
        //Delete Note 1 test
        mSolo.clickOnMenuItem("Delete");
        Spoon.screenshot(getActivity(), "1_note_deleted");
        //Note 1 test should not be found
        boolean noteFound = mSolo.searchText("Note 1 test");
        //Assert that Note 1 test is not found
        assertFalse("Note 1 Test is found", noteFound);
        mSolo.clickLongOnText("Note 2");
        //Clicks on Delete in the context menu
        mSolo.clickOnText("Delete");
        Spoon.screenshot(getActivity(), "2_note_deleted");
        //Will wait 100 milliseconds for the text: "Note 2"
        noteFound = mSolo.waitForText("Note 2", 1, 100);
        //Assert that Note 2 is not found
        assertFalse("Note 2 is found", noteFound);
        Spoon.screenshot(getActivity(), "final_state");
    }
}