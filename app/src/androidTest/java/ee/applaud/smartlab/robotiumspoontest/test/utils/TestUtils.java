package ee.applaud.smartlab.robotiumspoontest.test.utils;

import android.util.Log;

/**
 * Created by Dmitri Bogatenkov on 12/3/2015.
 */
public class TestUtils {

    private static final String LOG_TAG = "NotePadTest";
    private static final boolean DEBUG = true;

    public static void writeLogCat(final String text) {
        if (DEBUG)
            Log.i(LOG_TAG, text);
    }
}
