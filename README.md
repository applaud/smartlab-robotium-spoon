Smartlab test project using:
- spoon [JakeWharton https://github.com/square/spoon]
- robotium [https://code.google.com/p/robotium]

If you are a Windows OS user, please do not forget to add Android SDK to the environment variables. 
To run tests, please change build.bat file located in the folder named spoon and execute it.